from setuptools import setup

setup(name='mongocity',
      version='0.1',
      description='A simple MongoDB object mapper',
      url='https://bitbucket.org/imdugud/mongocity/',
      author='Tariq Daouda',
      author_email='tariq.daouda at umontreal dot ca',
      license='MIT',
      install_requires=['pymongo'],
      packages=['mongocity'],
      zip_safe=False)
