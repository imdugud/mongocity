from collections import defaultdict
from pymongo import MongoClient
from bson.objectid import ObjectId

#
#Mongocity is a small but fast module that let you enforce schema design and setup constraints on fields for mongodb

#checks performed on individual fields only when set one by one (fastest)
CRITICAL_LEVEL_LOW = 0
#Low level checks + a constitency check on all values before saving. Every field that is not part of the schema will be ignored during the save (default)
CRITICAL_LEVEL_NORMAL = 1
#Normal level checks + a constitency check on all values when initialasing with a dict (paranoid, slowest)
CRITICAL_LEVEL_HIGH = 100
#Same as high but will raise a KeyError for every field that is part of the schema while attempting to save(paranoid, annoying, slowest)
#CRITICAL_LEVEL_HIGH_VERBOSE = 101

class NotUnique(Exception) :
	
	def __init__(self, msg) :
		self.msg = msg
	
	def __str__(self) :
		return "NotUnique Exception: " + self.msg

class MongocityConfigure(object) :
	instance = None

	def __new__(cls, **kwargs) :
		if cls.instance :
			cls.instance._runInit = False
			return cls.instance

		cls.instance = object.__new__(cls, **kwargs)
		cls.instance._runInit = True
		return cls.instance
	
	def __init__(self, **kwargs) :
		if self._runInit:
			try :
				self.client = MongoClient(kwargs['client'])
				self.db = self.client[kwargs['database']]
			except KeyError :
				raise AttributeError("The first time you instanciate me, you must provide a \"client = ...\" and a \"database = ...\" for mongodb.")

class Mongocity_metaclass(type) :
	def __new__(cls, name, bases, attrs) :
		
		def _rec_bases(bases, attrs, fields, notNulls, _idAliases) :
			for base in bases :
				_rec_bases(base.__bases__, base.__dict__, fields, notNulls, _idAliases)
			
			for k, v in attrs.iteritems() :
				if v.__class__ is MongocityField :#and k not in fields :
					if k in _idAliases :
						_idAliases.remove(k)
					fields[k] = v
					if v.notNull :
						notNulls.add(k)
				elif v.__class__ is MongocityId :
					fields[k] = v
					_idAliases.add(k)
		
		fields = {}
		notNulls = set()
		_idAliases = set()
		_rec_bases(bases, attrs, fields, notNulls, _idAliases)

		for k, v in attrs.iteritems() :
			if v.__class__ is MongocityField :
				fields[k] = v
				if v.notNull :
					notNulls.add(k)

		attrs['fields'] = fields
		attrs['notNulls'] = notNulls
		attrs['_idAliases'] = _idAliases

		if ('_mongo_collection' not in attrs) or (not attrs['_mongo_collection']) :
			attrs['collection'] = None
		else :
			conf = MongocityConfigure()
			attrs['collection'] = conf.db[attrs['_mongo_collection']]

		return type.__new__(cls, name, bases, attrs)

def yesMan_filter(value) :
	"default filter for fields: no filter. A filter must raise a ConstraintViolation with an explanotary message if the value if not ok. In case of a valid value you can make it return whatever you want"
	return True

class ConstraintViolation(Exception) :
	def __init__(self, message, errors = {}) :
		Exception.__init__(self, message)
		self.errors = errors

	def __str__(self) :
		return self.message

class MongocityField(object) :
	"""Informs Mongocity that a class variable is a part of the schema"""
	def __init__(self, notNull = False, constraintFct = yesMan_filter) :
		self.notNull = notNull
		self.constraintFct = constraintFct

	def test(self, v) :
		if v != None :
			if not self.constraintFct(v) :
				raise ConstraintViolation("Violation of constraint fct: %s" %(self.constraintFct.func_name))
		
		if self.notNull :
			raise ConstraintViolation("This fields can't have a NULL value (\"None\" in python)")
		
		return True

	def __repr__(self) :
		return "<Mongocity field, not null: %s, constraint fct: %s>" %(self.notNull, self.constraintFct.func_name)

class MongocityId(MongocityField) :
	"""This class allows you to set an alias for _id field. Ex email = MongocityId() => _id can be accessed using the field email.
	-Aliases are virtual fields, they are simply other names for the _id field
	-A class can have several aliases
	-Aliases are inherited
	-Children can add new ones
	-Aliases in children can become redefined as fields and vice versa"""
	def __init__(self, *args, **kargs) :
		MongocityField.__init__(self, *args, **kargs)
		#self.mongoField = MongocityField(*args, **kargs)
	
	def __repr__(self) :
		return "<Mongocity _id alias, not null: %s, constraint fct: %s>" %(self.notNull, self.constraintFct.func_name)

class MongocityDict(object) :
	"""This dict like class for storing the values. It's main purpose is to provide a transparent handeling of the alias for _id"""
	
	def __init__(self, mongocityObj, dct) :
		self.mongocityObj = mongocityObj
		self.store = dct
	
	def getFormatedDict(self) :
		return self.formatDict(self.mongocityObj._idAliases, self.store)

	@classmethod
	def formatDict(cls, aliases, dct) :
		"removes the aliases form dct and replaces it by _id so it can be safely passed to mongo"
		for al in aliases :
			if al in dct and al != '_id':
				v = dct[al]
				dct["_id"] = v
				del(dct[al])
		return dct

	def __getitem__(self, k) :
		if k in self.mongocityObj._idAliases or k == '_id':
			try :
				return self.store["_id"]
			except KeyError :
				return None
			
		return self.store[k]

	def __setitem__(self, k, v) :
		if k in self.mongocityObj._idAliases :
			self.store["_id"] = v
		else :
			self.store[k] = v

	def __delitem__(self, k) :
		del(self.store[k])

	def __contains__(self, k) :
		return k in self.mongocityObj._idAliases or k in self.store

	def __getattr__(self, k) :
		return getattr(self.store, k)
	
	def __eq__(self, k) :
		try:
			return k.values == self.values
		except :
			return False

	def __str__(self) :
		return "%s, _idAliases: %s" %(str(self.store), str(list(self.mongocityObj._idAliases)))
	
class Mongocity(object) :
	"""Every model must inherit from this class.
	The name the collection is automatically created as mongocity_%s %(class name)

	special class fields:
	-_mongo_collection : a mongodb collection instanciated with pymongo. If the class does not have this field or if the value is None, the class is considered abstract and cannot be instanciated.
	-_mongocity_critical_level : sets the level of paranoia on fields consistency. Default is CRITICAL_LEVEL_NORMAL
	"""

	__metaclass__ = Mongocity_metaclass
	_mongocity_critical_level = CRITICAL_LEVEL_NORMAL

	_id = MongocityId()

	def __init__(self, values = {}) :
		if self.collection is None :
			raise ValueError("Type \"%s\" is abstract, I can not instanciate this object" % self.__class__.__name__)

		if self._mongocity_critical_level == CRITICAL_LEVEL_LOW :
			self.values = MongocityDict(self, dct = values)
		else :
			self.empty()
			self.set(values)
		
	def save(self) :
		if self.collection is None :
			raise ValueError("Type \"%s\" is abstract, I can not save this object in db" % self.__class__.__name__)
		
		if len(self.notNulls) > 0 :
			testNull = []
			for k in self.notNulls :
				try :
					if self.values[k] is None :
						testNull.append(k)
				except KeyError :
					testNull.append(k)

			if len(testNull) > 0 :
				raise ConstraintViolation("Unable to save \"%s\" object, following Non-NULL fields are \"None\" or have no values: %s" % (self.__class__.__name__, str(testNull)))

		if self._mongocity_critical_level > CRITICAL_LEVEL_LOW :
			saveValues = {}
			for k, v in self.values.store.iteritems() :
				if not self.hasField(k) :
					if self._mongocity_critical_level == CRITICAL_LEVEL_HIGH:
						raise KeyError("Can not save. Mongocity type \"%s\" does not have a field \"%s\"" % (self.__class__.__name__, k))
					if self._mongocity_critical_level == CRITICAL_LEVEL_NORMAL :
						print "Warning: Mongocity type \"%s\" does not have a field \"%s\" in it's schema" % (self.__class__.__name__, k)
				else :
					self.__class__.fields[k].test(v)
				saveValues[k] = v
			self.values['_id'] = self.collection.save(saveValues)
		else :
			self.values['_id'] = self.collection.save(self.getFormatedDict())

	def empty(self) :
		self.values = MongocityDict(self, {})

	def delete(self) :
		if "_id" not in self.values :
			raise KeyError("can't delete elemet without a field _id")
		self.collection.remove({'_id' : self['_id']})

	def set(self, dct, setEmpties = False) :
		"if setEmpties, the dct values that are None or '' will be applied. returns true if self has been modified, false if not"
		modified = False
		if dct is not None :
			for k, v in dct.iteritems() :
				if not self.hasField(k) :
					if self._mongocity_critical_level == CRITICAL_LEVEL_HIGH:
						raise KeyError("Can't set field. Mongocity type \"%s\" does not have a field \"%s\"" % (self.__class__.__name__, k))
					if self._mongocity_critical_level == CRITICAL_LEVEL_NORMAL :
						print "Warning: Mongocity type \"%s\" does not have a field \"%s\" in it's schema" % (self.__class__.__name__, k)
				if setEmpties or (v is not None and v != '' and self[k] != v) :
					self[k] = v
					modified = True
		return modified

	def __getitem__(self, k) :
		if not self.hasField(k) :
			raise KeyError("Mongocity type \"%s\" does not have a field \"%s\"" % (self.__class__.__name__, k))

		if k not in self.values :
			self.values[k] = None

		return self.values[k]

	def __setitem__(self, k, v) :
		if not self.hasField(k) :
			msg = "Mongocity type \"%s\" does not have a field \"%s\" in it's schema" % (self.__class__.__name__, k)
			if self._mongocity_critical_level > CRITICAL_LEVEL_NORMAL :
				raise KeyError(msg)
			elif self._mongocity_critical_level == CRITICAL_LEVEL_NORMAL :
				print "Warning: %s" % msg
		try :
			self.__class__.fields[k].test(v)
		except ConstraintViolation as e:
			raise ValueError("Mongocity type: \"%s\", field: \"%s\" => %s" %(self.__class__.__name__, k, str(e) ))
		except KeyError :
			pass
		
		self.values[k] = v
	
	def __getattribute__(self, k) :
		if object.__getattribute__(self, 'hasField')(k) :
			raise AttributeError("Type fields are accessible only through brackets : \"[]\"")
		return object.__getattribute__(self, k)

	def __getattr__(self, k) :
		return getattr(self.values, k)
	
	@classmethod
	def help(cls) :
		f = []
		for k, v in cls.fields.iteritems() :
			if v.__class__ is MongocityId :
				idAlias = "[_id Alias] "
			else :
				idAlias = ""
			f.append("|-> %s%s, not null: %s, constraint: %s" %(idAlias, k, v.notNull, v.constraintFct.__name__))
		return "Available fields for Mongocity type \"%s\":\n\t%s" %(cls.__name__, '\n\t'.join(f))

	@classmethod
	def hasField(cls, k) :
		return k in cls.fields

	@classmethod
	def find(cls, dct = {}, raw = False) :
		"as in mongo"
		
		for r in cls.collection.find(MongocityDict.formatDict(cls._idAliases, dct)) :
			if not raw :
				yield(cls(r))
			else :
				yield r
			
	@classmethod
	def find_one(cls, dct = {}) :
		"as in mongo"
		return cls(cls.collection.find_one(MongocityDict.formatDict(cls._idAliases, dct)))

	@classmethod
	def find_unique(cls, dct = {}) :
		"like find one but raises a NotUnique exception if there's more than one result"
		
		res = None
		for f in cls.find(MongocityDict.formatDict(cls._idAliases, dct)) :
			if res is not None:
				raise NotUnique('More than one value match the request')
			res = f
		
		if res is not None:
			return cls(res)
		return res
		
	@classmethod
	def count(cls, dct = {}) :
		return cls.collection.find(MongocityDict.formatDict(cls._idAliases, dct)).count()

	@classmethod
	def drop(cls) :
		"erase all documents and indexes"
		MongocityConfigure().db.drop_collection(cls.collection)
	
	@classmethod
	def flush(cls) :
		"erase all documents but keep indexes"
		cls.collection.remove()

	def __str__(self) :
		return "%s, collection: %s" % (str(self.values), self.collection.name)
