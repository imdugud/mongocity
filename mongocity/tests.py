from mongocity import *
MongocityConfigure(client = 'mongodb://localhost:27017/', database = "MongocityTest")

import unittest

def ageFilter(value) :
	return value > 13

class A(Mongocity) :
	email = MongocityId()
	name = MongocityField()
	age = MongocityField(constraintFct = ageFilter)

	_mongo_collection = 'A'
	def __init__(self, *args, **kwargs) :
		Mongocity.__init__(self, *args, **kwargs)

class B(A) :
	email = MongocityField()
	name = MongocityId()

	_mongo_collection = 'B'
	_mongocity_critical_level = CRITICAL_LEVEL_HIGH

	def __init__(self, *args, **kwargs) :
		Mongocity.__init__(self, *args, **kwargs)

class MongocityTests(unittest.TestCase):
	def setUp(self):
		pass
	
	def tearDown(self):
		B.drop()

	def test_alias(self) :
		name = 'Tariq Daouda'
		u = B({'email' : 'tariq.daouda@moonmoon.ca'})
		u['_id'] = name
		self.assertEqual(name, u['name'])

	def test_insert(self) :
		B.drop()
		u = B({'email' : 'tariq.daouda56@moonmoon.ca'})
		u['_id'] = 'Tariq Daouda'
		u['age'] = 28
		u.save()
		self.assertEqual(B.count(), 1)
	
	def test_drop(self) :
		B.drop()
		u = B({'email' : 'tariq.daouda56@moonmoon.ca'})
		u['_id'] = 'Tariq Daouda'
		u['age'] = 28
		u.save()
		B.drop()
		self.assertEqual(B.count(), 0)

	def test_insert_load(self) :
		B.drop()
		u = B({'email' : 'tariq.daouda56@moonmoon.ca'})
		u['_id'] = 'Tariq Daouda'
		u['age'] = 28
		u.save()
		u2 = B.find_one({'email' : 'tariq.daouda56@moonmoon.ca'})
		self.assertTrue((u["_id"] == u2["_id"]) and (u["age"] == u2["age"]) and (u["email"] == u2["email"]))
	
	def test_find_unique(self) :
		u = B({'email' : 'tariq.daouda56@moonmoon.ca'})
		u['_id'] = 'Tariq Daouda'
		u['age'] = 28
		u.save()
		u = B({'email' : 'tariq.daouda22@moonmoon.ca'})
		u['_id'] = 'Tariq Daouda II'
		u['age'] = 28
		u.save()
		
		self.assertRaises(NotUnique, B.find_unique, {'age' : 28})
	
	def test_update(self) :
		B.drop()
		u = B({'email' : 'tariq.daouda56@moonmoon.ca'})
		u['_id'] = 'Tariq Daouda'
		u['age'] = 28
		u.save()
		t = B.find_unique({'age' : 28})
		t["age"] = 27
		t.save()
		v = B.find_unique({'age' : 27})
		self.assertTrue(t['_id'] == u['_id'])

if __name__ == "__main__" :
	unittest.main()
